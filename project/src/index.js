// !TASK-1

import ReactDom from "react-dom/client";
// import React from "react";
// const text = "djskaldjskldjaiorwgmPDPasoijmomisoijdsaidjthemoisasdoonionbest";
// const pdpText = text.substring(20, 23);
// const isText = text.substring(31, 33);
// const theText = text.substring(42, 45);
// const bestText = text.substring(58, 62);

// ReactDom.createRoot(document.getElementById("root")).render(
//   <div>
//     <h1>
//       {pdpText} {isText} {theText} {bestText}
//     </h1>
//   </div>
// );

// ! 2-TASK

// class Home {
//   method1() {
//     console.log("something");
//   }
//   method2() {
//     console.log("hi");
//   }
//   method3() {
//     console.log("anything");
//   }
// }

// new Home().method1();

// ! Lesson

// class flowers {
//   constructor(name, color, originCountary) {
//     this.name = name;
//     this.color = color;
//     this.origin = originCountary;
//   }
// }

function flowers(name, color, originCountary) {
  this.name = name;
  this.color = color;
  this.origin = originCountary;
}

const tulip = new flowers("Tulip", "red", "UK");
const rose = new flowers("Rose", "yellow", "Russia");
const daisy = new flowers("Daisy", "white", "Switzerland");

console.log(tulip);
console.log(rose);
console.log(daisy);

// import React from "react";
// import ReactDOM from "react-dom/client";

// class Counter extends React.Component {
//   render() {
//     return <div>Counter</div>;
//   }
// }

// export default class App extends React.Component {
//   render() {
//     return (
//       <div>
//         <Counter />
//       </div>
//     );
//   }
// }

// const root = ReactDOM.createRoot(
//   document.getElementById("root").render(<App />)
// );
